# JavaScript对象
~~~js
  var wangxue={
            name:'王雪',
            birth:2000,
            school:'江河高级中学',
            height:1.65,
            weight:55,
            score:null
        };
        wangxue.school; //'江河高级中学'
        wangxue.birth; //2000

-- 访问属性是通过.操作符完成的，但这要求属性名必须是一个有效的变量名。如果属性名包含特殊字符，就必须用''括起来： 
      例： var wangxue={
           name:'王雪',
           'middle-school':'江河高级中学'
       };

--wangxue的属性名middle-school不是一个有效的变量，就需要用''括起来。访问这个属性也无法使用.操作符，必须用['xxx']来访问
     例：wangxue['middle-school']; //'江河高级中学'
     wangxue['name']; //王雪
     wangxue.naem; //王雪
~~~
~~~js
由于JavaScript的对象是动态类型，你可以自由地给一个对象添加或删除属性：
var wangxue={
    name:'王雪'
};
wangxue.age; //undefined
wangxue.age=20; //新增一个age属性

wangxue。age; //20
delete wangxue.age; //删除
wangxue.age; //undefined

delete wangxue['name']; //删除name属性
wangxue.name; //undefined
delete wangxue.school; //删除一个不存在的school属性也不会报错
~~~
~~~js
检测wangxue是否有某一种属性，用in操作符
var wangxue={
            name:'王雪',
            birth:2000,
            school:'江河高级中学',
            height:1.65,
            weight:55,
            score:null
        };
  'name' in wangxue; //true
  'sex' in wangxue; //false  

如果in判断一个属性存在，这个属性不一定是xiaoming的，它可能是wangxue继承得到的,判断方法hasOwnProperty()
'toString' in xiaoming; // true
var wangxue={
   name:'王雪'  
};
wangxeu.hasOwnProperty('name'); //true
wangxeu.hasOwnProperty('toString'); //flase 
~~~