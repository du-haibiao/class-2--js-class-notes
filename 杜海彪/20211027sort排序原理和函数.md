```js
    <script>
        var arr=[8,5,9,1,7];
        for(let i = 0; i<arr.length;i++){
            for(let k = 0; k<arr.length;k++){
                if(arr[i]<arr[k]){
                    var n = arr[k];
                    arr[k] = arr[i];
                    arr[i] = n;
                }
            }
        }
        console.log(arr);
    </script>
```
## sort 排序
排序中有一个重要的概念：稳定排序和不稳定排序。

稳定排序：大小相等的元素在被排序后前后位置关系不变

不稳定排序：大小相等的元素在被排序后前后位置关系可能改变

 
sort() 是不稳定排序，稳定排序函数是 stable_sort()

sort() 的排序对象可以是任何数据类型，int , __int64 ,  long long , char , double , string , 结构体……

1.默认的sort函数是按升序排，这时需要传递两个参数。

下面这句的作用是对数组元素a[0]~a[n-1]进行升序排序

sort(a,a+n);   //两个参数分别为待排序数组的首地址和尾地址

下面这句的作用是对数组元素a[1]~a[n]进行升序排序

sort(a+1,a+n+1);  //注意体会参数的含义和特点。