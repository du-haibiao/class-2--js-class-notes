# 函数
~~~js
在JavaScript中，定义函数的方式
function abs(x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
function指出这是一个函数定义；
abs是函数的名称；
(x)括号内列出函数的参数，多个参数以,分隔；
{ ... }之间的代码是函数体，可以包含若干语句，甚至可以没有任何语句。
~~~
~~~js
第二种定义函数的方式
var abs = function (x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
};
function (x) { ... }是一个匿名函数，它没有函数名。
但是，这个匿名函数赋值给了变量abs，所以，通过变量abs就可以调用该函数。
两种定义完全等价，注意第二种方式按照完整语法需要在函数体末尾加一个;，表示赋值语句结束
~~~
~~~js
调用函数
abs(10); // 返回10
abs(-9); // 返回9
传入的参数比定义的少也没有问题
abs(); // 返回NaN
此时abs(x)函数的参数x将收到undefined，计算结果为NaN。
避免收到undefind，对参数检查
function abs(x) {
    if (typeof x !== 'number') {
        throw 'Not a number';
    }
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
~~~
~~~js
// foo(a[, b], c)
// 接收2~3个参数，b是可选参数，如果只传2个参数，b默认为null：
function foo(a, b, c) {
    if (arguments.length === 2) {
        // 实际拿到的参数是a和b，c为undefined
        c = b; // 把b赋给c
        b = null; // b变为默认值
    }
    // ...
}
它只在函数内部起作用，并且永远指向当前函数的调用者传入的所有参数
'use strict'
function foo(x) {
    console.log('x = ' + x); // 10
    for (var i=0; i<arguments.length; i++) {
        console.log('arg ' + i + ' = ' + arguments[i]); // 10, 20, 30
    }
}
foo(10, 20, 30);
利用arguments，你可以获得调用者传入的所有参数。也就是说，即使函数不定义任何参数，还是可以拿到参数的值：
function abs() {
    if (arguments.length === 0) {
        return 0;
    }
    var x = arguments[0];
    return x >= 0 ? x : -x;
}

abs(); // 0
abs(10); // 10
abs(-9); // 9
~~~